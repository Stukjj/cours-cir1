<?php

define('TARGET_DIRECTORY', './photos/');
if(isset($_POST['add'])) {
  if((!empty($_POST['nameProd'])) && (!empty($_POST['price'])) && (!empty($_POST['number'])) && (!empty($_FILES['pict']))) {
    move_uploaded_file($_FILES['pict']['tmp_name'], TARGET_DIRECTORY . $_FILES['pict']['name']);
  }
  else {
    if(!empty($_POST['nameProd'])) {
      $_SESSION['nameProd'] = $_POST['nameProd'];
    }
    if(!empty($_POST['number'])) {
      $_SESSION['number'] = $_POST['number'];
    }
    if(!empty($_POST['price'])) {
      $_SESSION['price'] = $_POST['price'];
    }
    if(!empty($_FILES['pict'])) {
      $_SESSION['pict'] = $_FILES['pict'];
    }
    if(!empty($_POST['otherCom'])) {
      $_SESSION['otherCom'] = $_POST['otherCom'];
    }
    echo "<script>alert(\"Veuillez remplir tous les champs\")</script>";
  }
}
