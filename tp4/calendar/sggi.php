<?php

session_start();

$ini_array = parse_ini_file("./secret.ini", true);
try{
    $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
    $database = new PDO($ini_array['section']['dsn'], $ini_array['section']['user'], $ini_array['section']['password'], $opts);
} catch (Exception $e) {
        exit('Erreur de connexion à la base de donnée.'.$e->getMessage());
}

$data = $database->prepare('Select password,id,rank From Users Where login = ?');
$data->execute(array($_POST['username']));
$result = $data->fetch();
$isPasswordCorrect = password_verify($_POST['password'], $result['password']);

if($isPasswordCorrect) {
    $_SESSION['user'] = $_POST['username'];
    $_SESSION['id'] = $result['id'];
    $_SESSION['rank_user'] = $result['rank'];
    header('Location: /calendar/calendar.php');
    exit();
}
else {
    $error = 1;
    include('./sign in.php');
    exit();
}